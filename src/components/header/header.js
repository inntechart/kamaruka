import React, { useState } from 'react';
import logo from '../../../public/images/kamaruka_logo.png';
import headerBg from '../../../public/images/header-bg.png';
import {Link, NavLink } from "react-router-dom";
import './header.scss'

const Header = () => {

    const [isOpenMenu, setIsOpenMenu] = useState(false);

    const toggleMenu = () => {
      setIsOpenMenu(!isOpenMenu);
    };

    return (
        <>
            <div className={"row nav-desktop d-none d-md-block"}>
                <div className="col-12">
                    <div className="row logo-info-content">
                        <NavLink to={'/'} className="col-auto  d-md-block d-none">
                            <img className={"logo"} src={logo} alt="img"/>
                        </NavLink>
                        <div className={"d-flex align-items-center brand col justify-content-center text-left text-md-center"}>
                            <div>
                                <h1 className={"brand-name mb-0"}>KAMARUKA</h1>
                                <h3 className={"brand-educ"}>EDUCATION CENTRE</h3>
                            </div>
                        </div>
                        <div className={"col-auto float-right phone_search"}>
                            <div className={"phone"}>
                                <div className={"phone-number"}>
                                    03 9826 0330
                                </div>
                            </div>
                            <form className="form-inline search-form mt-3">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                                <button type="submit">
                                    <i className="fa fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col-12 p-0">
                    <nav className="menu w-100 text-center bg-dark">
                        <ul className="nav justify-content-center">
                            <li className="nav-item">
                                <NavLink activeClassName="active" className="nav-link" to={'/about'}>About <span
                                    className="sr-only">(current)</span></NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName="active" className="nav-link" to={'/philosophy'}>Philosophy</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName="active" className="nav-link" to={'/curriculum'}>Curriculum</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName="active" className="nav-link" to={'/approach'}>Approach</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName="active" className="nav-link" to={'/enrolment'}>Enrolment</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName="active" className="nav-link" to={'/payments'}>Payments</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName="active" className="nav-link" to={'/contact'}>Contact</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName="active" className="nav-link" to={'/home'}>Home</NavLink>
                            </li>
                        </ul>
                    </nav>
                    <nav className="sub-menu w-100 text-center">
                        <ul className="nav justify-content-center">
                            <li className="nav-item active">
                                <NavLink  activeClassName="active" className="nav-link" to={'/about/introduction'}>Introduction <span
                                    className="sr-only">(current)</span></NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink  activeClassName="active" className="nav-link" to={'/about/mission'}>Mission Statement</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink  activeClassName="active" className="nav-link" to={'/about/school-performance'}>School Performance</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink  activeClassName="active" className="nav-link" to={'/approach'}>Staff</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink  activeClassName="active" className="nav-link" to={'/enrolment'}>Committee</NavLink>
                            </li>
                        </ul>
                    </nav>

                </div>

                <div className="col-12 p-0">
                    <img src={headerBg} className="header-img w-100" alt="header-img"/>
                </div>
            </div>
            <div className="row nav-mobile d-md-none">
                <div className="col-12 nav-header">
                    <div className="row h-100">
                        <div className="col align-items-center d-flex">
                            <h1><NavLink  activeClassName="active" to={'/'}>KAMARUKA</NavLink></h1>
                        </div>
                        <div className="align-items-center col-auto d-flex justify-content-center">
                            <div className="row">
                                <div className="align-items-center col d-flex justify-content-end text-right">
                                    <i className="fa fa-search"></i>
                                </div>
                                <div className="col-auto">
                                    <button className="btn btn-toggle-menu bg-dark text-white" onClick={() => toggleMenu()}>Menu</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 p-0">
                    <div className="header-img w-100" />
                </div>
                {isOpenMenu &&
                <nav className="col-12 bg-dark">
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <NavLink activeClassName="active" className="nav-link" to={'/about'}>About</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink  activeClassName="active" className="nav-link" to={'/philosophy'}>Philosophy</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink  activeClassName="active" className="nav-link" to={'/curriculum'}>Curriculum</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink  activeClassName="active" className="nav-link" to={'/approach'}>Approach</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink  activeClassName="active" className="nav-link" to={'/enrolment'}>Enrolment</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink  activeClassName="active" className="nav-link" to={'/payments'}>Payments</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink  activeClassName="active" className="nav-link" to={'/contact'}>Contact</NavLink>
                        </li>
                        <li className="nav-item active">
                            <NavLink  activeClassName="active" className="nav-link" to={'/'}>Home Page</NavLink>
                        </li>
                    </ul>
                </nav>}

            </div>
        </>
    );
};

export default Header;