import React, { useState, useEffect } from 'react';
import {Link} from "react-router-dom";
import './footer.scss'
import arrow from '../../../public/images/icons/arrow.png';

const Footer = () => {
    const toTop = () => {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth' });
    };

    return (
        <footer className="footer row">
            <div className="col-12 d-md-none">
                <div className="row">
                    <form className="form-inline search-form mt-3 col-12">
                        <input className="form-control mr-sm-2 w-100" type="search" placeholder="Search" aria-label="Search"/>
                        <button type="submit">
                            <i className="fa fa-search"></i>
                        </button>
                    </form>
                    <div className="col-12 contact-block mt-3">
                        <h3>Contact</h3>
                        <div className="row contact">
                            <p className="col-12 title">Phone</p>
                            <span className="col-5">Australia</span>
                            <span className="col-7">03 9826 0330</span>
                            <span className="col-5">International</span>
                            <span className="col-7">+61 3 9826 0330</span>
                            <div className="col-12 mt-2">
                                <a className="btn btn-phone w-100" href="tel:03 9826 0330">03 9826 0330</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 contact-block mt-3">
                        <h3>Contact</h3>
                        <div className="row contact">
                            <p className="col-12 title">Address</p>
                            <span className="col-12 font-weight-bold">Secondary School</span>
                            <span className="col-6">52 River Street South Yarra VIC 3141 Australia</span>
                            <span className="col-12 font-weight-bold">Primary Annexe</span>
                            <span className="col-6">
                                38-40 River Street
                                South Yarra VIC 3141
                                Australia
                            </span>
                            <div className="col-12 mt-2">
                                <a className="btn btn-map w-100" href="#">MAP</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 contact-block mt-3">
                        <h3>Online</h3>
                        <div className="row contact">
                            <a className="col-12" href="mailto:info@kamaruka.vic.edu.au">info@kamaruka.vic.edu.au</a>
                            <div className="col-12 mt-2">
                                <a className="btn btn-enquiry-form w-100" href="#">ENQUIRY FORM</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 d-flex justify-content-center py-4">
                        <div>
                            <img src={arrow} alt="arrow"/>
                            <p className="mb-0 text-uppercase" onClick={()=> toTop()}>top</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav className="policy w-100">
                <ul className="nav justify-content-center">
                    <li className="nav-item active">
                        <Link className="nav-link" to={'/about'}>Privacy <span className="d-none d-md-inline-block"> Policy</span></Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to={'/philosophy'}>
                            <span className="d-none d-md-inline-block">Terms & </span> Conditions</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to={'/curriculum'}>Child Safety <span className="d-none d-md-inline-block"> Policy</span></Link>
                    </li>
                </ul>
            </nav>
            <div className={"footer-text col-12"}>
                <div className="row mt-4">
                    <p className="col-12 info text-center">Kamaruka is a registered charity listed under the Australian Charities and Not-for-profits Commission (ACNC) <span className="d-none d-md-inline-block">   |  </span>  <span className="d-block d-md-inline-block  mt-3">ABN: 82 267 854 859</span> </p>
                    <p className="col-12 cooperation-text text-center">Copyright © Kamaruka Inc 2000 - 2020</p>
                </div>
                <div className="row justify-content-center mt-3 site-creator">
                    <div className="col-7 text-center">
                        <a href="#">uxdesign.melbourne</a>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;