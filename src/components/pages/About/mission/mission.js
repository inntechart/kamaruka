import React from 'react';
import "./mission.scss";
import donations from '../../../../../public/images/donations.png';
import arrow from "../../../../../public/images/icons/arrow.png";

const Mission = () => {
    const toTop = () => {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
    };

    return (
        <div className="container py-5">
            <div className="row hero-items">
                <div className="col-lg-8 col-md-7 about-text">
                    <h3 className="title">Mission Statement</h3>
                    <div>
                        <article className="mt-3">
                            Kamaruka is committed to promoting the principles and practice of Australian democracy.
                        </article>
                        <article className="mt-3">
                            The social skills and behavioural programs promote the values of openness and tolerance,
                            freedom of speech and association, freedom of religion and equal rights.
                        </article>
                        <article className="mt-3">
                            The teachings at Kamaruka support an elected government and the rule of law.
                        </article>
                        <article className="mt-3">
                            The social skills and behavioural programs promote the values of openness and tolerance,
                            freedom of speech and association, freedom of religion and equal rights.
                        </article>
                    </div>
                </div>
                <div className="col-lg-4 col-md-7 item">
                    <div className="w-100 image ">
                        <div
                            className='align-items-center d-flex img-transparent-background justify-content-center text-center'>

                        </div>
                        <div className="image p-5"
                             style={{backgroundImage: "url('/public/images/cover/20-years.png')"}}/>
                    </div>
                </div>
                <div className="col-lg-4 col-md-7 item">
                    <div className="w-100">
                        <div
                            className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-green-dark'>
                            <div>
                                <p>Here to Help</p>
                                <p>Positive choice change</p>
                            </div>
                        </div>
                        <div className="image bg-img"
                             style={{backgroundImage: "url('/public/images/cover/here-to-help.png')"}}/>
                    </div>
                </div>
                <div className="col-lg-4 col-md-7 item">
                    <div className="w-100">
                        <div
                            className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-blue'>
                            <div>
                                <p>Enrolment</p>
                                <p>Process</p>
                            </div>
                        </div>
                        <div className="image bg-img"
                             style={{backgroundImage: "url('/public/images/cover/enrolment-process.png')"}}/>
                    </div>
                </div>
                <div className="col-lg-4 col-md-7 item">
                    <div className="w-100">
                        <div
                            className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-orange'>
                            <div>
                                <p>Intervention</p>
                                <p>Program</p>
                            </div>
                        </div>
                        <div className="image bg-img"
                             style={{backgroundImage: "url('/public/images/cover/Intervention.png')"}}/>
                    </div>
                </div>
                <div className="col-lg-8 col-md-7 col-padding-3">
                    <div className="row about-donation-items">
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center'>
                                    <div>
                                        <h3>Here to Help</h3>
                                        <p>Positive choice change</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/here-to-help.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Our</h3>
                                        <h3>Strategy</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Modified</h3>
                                        <h3>Curriculum</h3>
                                        <p>Tailored for each student</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Literacy</h3>
                                        <h3>Intervention</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center">
                                    <div>
                                        <h3>Term</h3>
                                        <h3>Dates</h3>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/term-dates.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>ADHD & ASD</h3>
                                        <p>Self-management</p>
                                        <p>for success</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Results</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Enquiry Form</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center">
                                    <div>
                                        <h3>Payments</h3>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/payments.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Careers</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Map</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Contact</h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="col-lg-4 col-md-5 col-padding-3">
                    <div className="w-100 donations">
                        <div className="row">
                            <div className="col-8">
                                <h3>Donations</h3>
                                <h4 className="mt-4">We are saving to buy our own premises but we need your
                                    help!</h4>
                                <p className="mt-4">
                                    Kamaruka is a registered charity, so your donation is a tax deductible expense
                                    which
                                    means you can reduce your taxable income and lower your tax bill.
                                </p>
                            </div>
                            <div className="col-4">
                                <img src={donations} alt=""/>
                            </div>
                            <div className="col-12 mt-3">
                                <button type="button" className="btn btn-success w-100">Click to make a Donation
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 mt-5 d-none d-md-block text-center">
                    <div className="cursor-pointer w-auto" onClick={() => toTop()}>
                        <img src={arrow} alt="arrow"/>
                        <p className="mb-0 text-uppercase">top</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Mission;