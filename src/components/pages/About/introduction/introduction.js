import React from 'react';
import "./introduction.scss";
import donations from '../../../../../public/images/donations.png';
import arrow from "../../../../../public/images/icons/arrow.png";

const AboutIntroduction = () => {
    const toTop = () => {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth' });
    };

    return (
        <div className="container py-5">
            <div className="row">
                <div className="col-lg-8 col-md-7 about-text">
                    <h3 className="title">Introduction</h3>
                    <div>
                        <article className="mt-3">
                            Kamaruka Education Centre was founded in 2001 by the current principal, Alfonso Scibilia, to
                            cater for a unique group of boys whose needs could not be accommodated in the mainstream
                            classroom.
                        </article>
                        <article className="mt-3">
                            The school’s original campus in Richmond operated for 12 years accommodating up to 20
                            students. In response to the growing demand for Kamaruka’s intervention program offered, the
                            school relocated to a larger premises at 52 River Street South Yarra in 2013.
                        </article>
                        <article className="mt-3">
                            Further growth saw the establishment of a second premises in 2017 for the Primary Annex at
                            38 River street – four doors down. Kamaruka Education Centre now offers its alternative
                            intervention program for up to 60 students from years 3 to 10.
                        </article>
                        <article className="mt-3">
                            The program offered at Kamaruka is designed for boys diagnosed with Attention Deficit
                            Hyperactivity Disorder (ADHD), but can also benefit a select group of boys diagnosed with
                            high functioning autism spectrum disorder (ASD), formerly known as Asperger’s.
                        </article>
                        <article className="mt-3">
                            A commonality amongst these boys is the low self-esteem developed over several years of
                            failure in mainstream schools.
                        </article>
                        <article className="mt-3">
                            Their social inadequacies and emotional issues threaten their academic progress and social
                            inclusion in our society.
                        </article>
                        <article className="mt-3">
                            Through positive experiences and sense of belonging, self-esteem is restored and a pathway
                            towards a rewarding career becomes achievable. Co-morbid disorders such as Oppositional
                            Defiant Disorder (ODD) and Conduct Disorder (CD) often compromise their progress, as does
                            the anxiety which many of these boys present with at enrolment.
                        </article>
                        <article className="mt-3">
                            The word Kamaruka derives from an aboriginal term meaning “Camp of the spirit children”.
                        </article>
                        <article className="mt-3">
                            The three arrows of the Kamaruka emblem symbolise the core values targeted in the social
                            development of the students at Kamaruka which are:
                        </article>
                        <article className="mt-3">
                            <span className="mb-0">– Respect for self</span>
                            <span className="mb-0">– Respect for others</span>
                            <span className="mb-0">– Responsibility for one’s actions</span>
                        </article>
                        <article className="mt-3">
                            Our social skills and behavioural programs aim to promote these values.
                        </article>
                        <article className="mt-3">
                            Kamaruka Education Centre is committed to providing the best environment for your son to
                            maximise his potential .
                        </article>
                        <article className="mt-3">
                            The school prides itself in its values-based school culture, which creates a safe and caring
                            learning environment for students, staff and families.
                        </article>
                    </div>
                </div>
                <div className="col-lg-4 col-md-5">
                    <div className="row hero-items">
                        <div className="col-12 item">
                            <div className="w-100 image ">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center'>

                                </div>
                                <div className="image p-5"
                                     style={{backgroundImage: "url('/public/images/cover/20-years.png')"}}/>
                            </div>
                        </div>
                        <div className="col-12 item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-green-dark'>
                                    <div>
                                        <p>Here to Help</p>
                                        <p>Positive choice change</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/here-to-help.png')"}}/>
                            </div>
                        </div>
                        <div className="col-12 item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-blue'>
                                    <div>
                                        <p>Enrolment</p>
                                        <p>Process</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/enrolment-process.png')"}}/>
                            </div>
                        </div>
                        <div className="col-12 item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-orange'>
                                    <div>
                                        <p>Intervention</p>
                                        <p>Program</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/Intervention.png')"}}/>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="col-lg-8 col-md-7">
                    <div className="row about-donation-items">
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center'>
                                    <div>
                                        <h3>Here to Help</h3>
                                        <p>Positive choice change</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/here-to-help.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Our</h3>
                                        <h3>Strategy</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Modified</h3>
                                        <h3>Curriculum</h3>
                                        <p>Tailored for each student</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Literacy</h3>
                                        <h3>Intervention</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center">
                                    <div>
                                        <h3>Term</h3>
                                        <h3>Dates</h3>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/term-dates.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>ADHD & ASD</h3>
                                        <p>Self-management</p>
                                        <p>for success</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Results</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Enquiry Form</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center">
                                    <div>
                                        <h3>Payments</h3>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/payments.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Careers</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Map</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Contact</h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="col-lg-4 col-md-5 col-padding-3">
                    <div className="w-100 donations">
                        <div className="row">
                            <div className="col-8">
                                <h3>Donations</h3>
                                <h4 className="mt-4">We are saving to buy our own premises but we need your
                                    help!</h4>
                                <p className="mt-4">
                                    Kamaruka is a registered charity, so your donation is a tax deductible expense
                                    which
                                    means you can reduce your taxable income and lower your tax bill.
                                </p>
                            </div>
                            <div className="col-4">
                                <img src={donations} alt=""/>
                            </div>
                            <div className="col-12 mt-3">
                                <button type="button" className="btn btn-success w-100">Click to make a Donation
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 mt-5 d-none d-md-block text-center" onClick={()=> toTop()}>
                    <img src={arrow} alt="arrow"/>
                    <p className="mb-0 text-uppercase">top</p>
                </div>
            </div>
        </div>
    );
};

export default AboutIntroduction;