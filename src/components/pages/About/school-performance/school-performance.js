import React, {useState} from 'react';
import "./school-performance.scss";
import donations from '../../../../../public/images/donations.png';
import arrowDown from '../../../../../public/images/icons/arrow-down.png';
import arrow from '../../../../../public/images/icons/arrow.png';

const SchoolPerformance = () => {

    let reports = [
        {
            title: 'Background Information',
            subCategory: [
                {
                    id: 1,
                    title: "Philosophy & Rationale",
                    desc: "Kamaruka was established in 2001 to provide a more appropriate educational environment for boys diagnosed with Attention Deficit Hyperac tivity Disorder (ADHD).\n" +
                        "\n" +
                        "As the name suggests, attention deficit is a problem with concentration.\n" +
                        "\n" +
                        "The mainstream classroom, typically with 20 or more students, cannot provide the environment conducive to learning for boys with this disorder. Their need for a consistent approach to discipline and structure in their lives is not possible with the diversity of teaching styles they would encounter in a regular school setting.\n" +
                        "\n" +
                        "With a staff/student ratio of approximately 5:1 in secondary and 3:1 in primary classes, boys who have fallen behind academically can be given personalised attention to overcome their weaknesses and restore their self-esteem.\n" +
                        "\n" +
                        "The timetable is deliberately weighted towards physical activity and academic classes are interspersed with a more “hands on” activity to reduce the length of time the boys have to sit and concentrate."
                },
                {
                    id: 2,
                    title: "Intensive Intervention Program",
                    desc: "Over a period of two or three years, the intervention program at Kamaruka aims to address the social inadequacies and emotional issues associated with learning difficulties. \n" +
                        "\n" +
                        "Behavioural change is accomplished through a system of rewards and consequences, challenging boys to accept responsibility for the choices they make.\n" +
                        "\n" +
                        "Boys between the ages of 8 to 15 years, who have low-esteem, but with average or above average IQ, will benefit immensely from the positive experiences and a sense of belonging.\n" +
                        "\n" +
                        "The social skills training, strategies to self-manage behaviour and develop organisational skills, all in a very structured school environment, make the program particularly suited to boys exhibiting symptoms of Attention Deficit Hyperactivity Disorder (ADHD)."
                },
                {
                    id: 3,
                    title: 'Curriculum',
                    desc: 'It is usual for enrolments at Kamaruka to have a history of behavioural issues and consequently are already under the guidance and treatment by a health professional. \n' +
                        '\n' +
                        'Psychological assessments and school reports allow us to place students in appropriate classes to meet their individual academic and social needs. Following the VELS guidelines, the modified curriculum focuses on literacy & numeracy skills. \n' +
                        '\n' +
                        'Individual deficiencies are addressed with personalised remedial work for low achievers, and extension for those needing a more challenging curriculum.\n' +
                        '\n' +
                        'Language skills are fundamental to the growth of self-esteem. The ability to express ideas and feelings is vital for the personal and social development of the individual.\n' +
                        '\n' +
                        'The English curriculum is designed to improve reading fluency, comprehension, handwriting skills, expressive language, spelling and grammar.\n' +
                        '\n' +
                        'The curriculum includes English, mathematics, science, art, drama, music, gymnastics, kickboxing, jiu-jitsu, swimming and outdoor education via four camps throughout the year.'
                }
            ]
        },
        {
            title: 'Students',
            subCategory: [
                {
                    id: 4,
                    title: 'Enrolment & Attendance',
                    desc: "At the August Census in 2017, Kamaruka had an enrolment of 39 students (18 primary and 21 secondary).\n" +
                        "\n" +
                        "Procedures are in place, at Kamaruka, to ensure that school attendance is regular and students are punctual. Selective enrolments aim to create an environment of motivated students focusing on academic achievement. Students with a history of school refusal are unlikely to succeed in the Kamaruka program and are rarely enrolled.\n" +
                        "\n" +
                        "As the primary enrolments have been progressively increasing, it was decided that a primary annex should be established to separate the primary students from the negative influence of secondary boys. \n" +
                        "\n" +
                        "The Primary Annex (the ground floor at 38 River St 4 doors down from the main building) was opened at the start of the school year in February 2016. Late in 2017, the first floor became available and was leased, with the intention of moving the primary classes to a more spacious environment in 2018, thus accommodating more students.\n" +
                        "\n" +
                        "A number of the students at Kamaruka attend counseling during school hours on a regular basis which contributes to the absence statistics.\n" +
                        "\n" +
                        "Accounting for these absences as well as sick days, and special family holidays, the School Student Attendance Report for 2017 was as follows:",
                    semesters: [
                        {
                            date: "Year 3",
                            semester_one: "100%",
                            semester_two: "96%"
                        },
                        {
                            date: "Year 4",
                            semester_one: "92%",
                            semester_two: "87%"
                        },
                        {
                            date: "Year 5",
                            semester_one: "91%",
                            semester_two: "91%"
                        },
                        {
                            date: "Year 6",
                            semester_one: "79%",
                            semester_two: "70%"
                        },
                        {
                            date: "Year 7",
                            semester_one: "82%",
                            semester_two: "86%"
                        },
                        {
                            date: "Year 8",
                            semester_one: "94%",
                            semester_two: "88%"
                        },
                        {
                            date: "Year 9",
                            semester_one: "94%",
                            semester_two: "89%"
                        },
                        {
                            date: "Year 10",
                            semester_one: "90%",
                            semester_two: "88%"
                        }
                    ]
                },
                {
                    id: 5,
                    title: 'Transition & Reintegration',
                    desc: "Of the 35 boys who completed the Kamaruka program in 2017, 26 returned to Kamaruka in 2018, and 9 moved on to either:"
                },
                {
                    id: 6,
                    title: 'Naplan Testing',
                    desc: 'The Kamaruka program targets students with low self-esteem and social/emotional difficulties. It is our belief that subjecting such children to national testing procedures would only exacerbate their condition. \n' +
                        '\n' +
                        'Consequently, with parental support, students enrolled at Kamaruka are granted exemption from such testing.'
                },
            ]
        },
        {
            title: 'Staff',
            subCategory: [
                {
                    id: 7,
                    title: 'Staff Attendance & Retention',
                    desc: 'The teaching staff at Kamaruka are committed to maintaining a stable environment with minimal disturbance to the daily routine. \n' +
                        '\n' +
                        'The Staff attendance for 2017 was 96%. Building a rapport with students and understanding each student’s individual needs is vital to the successful implementation of the behavioural program. \n' +
                        '\n' +
                        'Being a small staff, regular briefings guarantee a unified approach to teaching and the use of consistent consequences in maintaining classroom discipline.'
                },
                {
                    id: 8,
                    title: 'Staff Qualifications',
                    desc: 'All teaching staff are registered with the Victorian Institute of Teaching.\n' +
                        '\n' +
                        'Specialist staff and teacher’s aide all have a current Working with Children Check.'
                },
                {
                    id: 9,
                    title: 'Professional Development',
                    desc: 'Teachers are encouraged to participate in professional development relevant to their subject area & to the objectives of the Kamaruka program. \n' +
                        '\n' +
                        'However, being a small school with a specialist program, it is difficult to release staff to attend conferences and seminars held during school hours.\n' +
                        '\n' +
                        'Children diagnosed with ADHD and the associated social/emotional difficulties, react adversely to changes in their daily routine. The distress suffered by even one student at the absence of a teacher, can reverberate throughout the school in a ripple effect. \n' +
                        '\n' +
                        'It is a futile and counterproductive exercise to send staff to a PD event intended to help the teaching process, if the efficacy of the program is compromised.\n' +
                        '\n' +
                        'To minimize the disruption to the school program, PD days are included on 4 occasions throughout the year. \n' +
                        '\n' +
                        'Each term, a school camp is held from Monday to Thursday, and the Friday is a pupil free day, during which professional development is scheduled.\n' +
                        '\n' +
                        'In 2017, staff attended the following PD days:\n'
                },
            ]
        },
        {
            title: 'Committee',
            subCategory: [
                {
                    id: 10,
                    title: 'Committee of Management',
                    desc: 'The Advisory Committee meets at least once per term to review policies and oversee the general operation of the school. The elected members for 2017 were as follows:'
                },
            ]
        },
        {
            title: 'Funding',
            subCategory: [
                {
                    id: 11,
                    title: 'Overview',
                    desc: 'Australian Government Targeted Programs\n' +
                        'Kamaruka relies heavily on Special Education Funding to run an effective intervention program. Most students enrolled before the end of Term 1 qualify for this funding for social/emotional disorder.\n' +
                        '\n' +
                        'Student enrolments are accepted throughout the year at Kamaruka, but are not able to access that funding after the end of Term 1.'
                },
                {
                    id: 12,
                    title: 'Grants',
                    desc: 'In 2017 Kamaruka received $836,209.00 from the Commonwealth Government General Recurrent Grants Program and $379,383.00 from the State Government Recurrent Grants.'
                },
            ]
        },
        {
            title: 'Program Satisfaction',
            subCategory: [
                {
                    id: 13,
                    title: 'Conclusion',
                    desc: 'Since the opening of the school in 2001, there has never been a substantiated complaint raised by any parent or staff member on any aspect of the school operation. We have had the occasional disappointed parent for whose son the program did not suit. \n' +
                        '\n' +
                        'A number of students who present with comorbid disorders and mental health issues beyond the scope of our program have been given an opportunity at Kamaruka since no other educational setting can accommodate their needs. \n' +
                        '\n' +
                        'Some of these students do not respond to our program and have to find an alternative setting for their education. Anecdotal reports from parents and health professionals reinforce the effectiveness of the structured program at Kamaruka. \n' +
                        '\n' +
                        'It is not uncommon to have parents report that students are generally more settled and are achieving better academically within a few weeks after enrolment.'
                },
            ]
        },
    ];

    const [reportList, setReportList] = useState(reports);
    const [collapsedReports, setCollapsedReports] = useState([]);
    const [isCollapseAll, setCollapseAll] = useState(false);

    const collapse = (id, isShow) => {
        if (isShow) {
            let collapsed = [...collapsedReports, id];
            setCollapsedReports(collapsed);
        } else {
            let collapsed = [...collapsedReports];
            collapsed = collapsed.slice(1, collapsed.indexOf(id));
            setCollapsedReports(collapsed);
        }

    };

    const collapseAll = () => {
        setCollapseAll(!isCollapseAll)
    };

    const toTop = () => {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
    };

    return (
        <div className="container py-5">
            <div className="row">
                <div className="col-lg-8 col-md-7 about-text">
                    <h3 className="title">School Performance</h3>
                    <p>School Performance Report - 2018</p>
                    <div className="row">
                            { reportList && reportList.map((item, index) =>{
                              return(
                                  <div className="col-12 col-padding-3 mt-4" key={index}>
                                      {item.title}
                                         {item.subCategory.map((cat, index)=> {
                                             return (
                                                 <div className="report-item" key={index}>
                                                     <div className="collapse show">
                                                         <div className="d-flex justify-content-between">
                                                             <div className="align-items-center d-flex">{cat.title}</div>
                                                             {
                                                                 !collapsedReports.includes(cat.id)  &&
                                                                 <div className="align-items-center cursor-pointer d-flex" onClick={() => collapse(cat.id, true)}>
                                                                     <span className="text-uppercase mr-2">more</span>
                                                                     <img src={arrowDown} alt="arrow-down"/>
                                                                 </div>
                                                             }
                                                         </div>
                                                         <div className={`collapse mt-4 ${collapsedReports.includes(cat.id) || isCollapseAll ? "show" : ""}` }>
                                                             {cat.desc}
                                                             {
                                                                 cat.semesters && <div className="mt-3 semesters">
                                                                     <table>
                                                                         <thead>
                                                                         <tr>
                                                                             <th className="text-right"
                                                                                 scope="col"></th>
                                                                             <th className="text-right"
                                                                                 scope="col"> Semester 1
                                                                             </th>
                                                                             <th className="text-right"
                                                                                 scope="col">Semester 2
                                                                             </th>
                                                                         </tr>
                                                                         </thead>
                                                                         <tbody>
                                                                         {cat.semesters.map((semester,index) =>
                                                                             <tr key={index}>
                                                                                 <td className="text-right">{semester.date}</td>
                                                                                 <td className="text-right">{semester.semester_one}</td>
                                                                                 <td className="text-right">{semester.semester_two}</td>
                                                                             </tr>
                                                                         )}
                                                                         </tbody>
                                                                     </table>
                                                                 </div>
                                                             }
                                                             <div className="d-flex justify-content-center mb-0 mt-3">
                                                                 <div className="text-center cursor-pointer" onClick={() => collapse(cat.id, false)}>
                                                                     <img src={arrow} alt=""/>
                                                                     <p className="text-uppercase mb-0 mt-1">Less</p>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             )
                                         })}
                                  </div>
                              )
                            })}
                            <div className="col-12 d-flex justify-content-center mb-0 my-4 collapse-btn">
                                <div className="text-center cursor-pointer" onClick={() => collapseAll()}>
                                    <img src={isCollapseAll ? arrow : arrowDown} alt=""/>
                                    <p className="text-uppercase mb-0 mt-1">{`${isCollapseAll ? "COLLAPSE" : "EXPAND"}`} <br/> ALL</p>
                                </div>
                            </div>
                    </div>
                    <div className="row about-donation-items">
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center'>
                                    <div>
                                        <h3>Here to Help</h3>
                                        <p>Positive choice change</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/here-to-help.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Our</h3>
                                        <h3>Strategy</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Modified</h3>
                                        <h3>Curriculum</h3>
                                        <p>Tailored for each student</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Literacy</h3>
                                        <h3>Intervention</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center">
                                    <div>
                                        <h3>Term</h3>
                                        <h3>Dates</h3>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/term-dates.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>ADHD & ASD</h3>
                                        <p>Self-management</p>
                                        <p>for success</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Results</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Enquiry Form</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center">
                                    <div>
                                        <h3>Payments</h3>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/payments.png')"}}/>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Careers</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Map</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 donation-item">
                            <div className="w-100">
                                <div
                                    className="align-items-center d-flex img-transparent-background justify-content-center text-center bg-transparent">
                                    <div>
                                        <h3>Contact</h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="col-lg-4 col-md-5">
                    <div className="row hero-items">
                        <div className="col-12 item">
                            <div className="w-100 image ">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center'>

                                </div>
                                <div className="image p-5"
                                     style={{backgroundImage: "url('/public/images/cover/20-years.png')"}}/>
                            </div>
                        </div>
                        <div className="col-12 item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-green-dark'>
                                    <div>
                                        <p>Here to Help</p>
                                        <p>Positive choice change</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/here-to-help.png')"}}/>
                            </div>
                        </div>
                        <div className="col-12 item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-blue'>
                                    <div>
                                        <p>Enrolment</p>
                                        <p>Process</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/enrolment-process.png')"}}/>
                            </div>
                        </div>
                        <div className="col-12 item">
                            <div className="w-100">
                                <div
                                    className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-orange'>
                                    <div>
                                        <p>Intervention</p>
                                        <p>Program</p>
                                    </div>
                                </div>
                                <div className="image bg-img"
                                     style={{backgroundImage: "url('/public/images/cover/Intervention.png')"}}/>
                            </div>
                        </div>
                        <div className="col-12 item">
                            <div className="w-100 donations">
                                <div className="row">
                                    <div className="col-8">
                                        <h3>Donations</h3>
                                        <h4 className="mt-4">We are saving to buy our own premises but we need your
                                            help!</h4>
                                        <p className="mt-4">
                                            Kamaruka is a registered charity, so your donation is a tax deductible
                                            expense
                                            which
                                            means you can reduce your taxable income and lower your tax bill.
                                        </p>
                                    </div>
                                    <div className="col-4">
                                        <img src={donations} alt=""/>
                                    </div>
                                    <div className="col-12 mt-3">
                                        <button type="button" className="btn btn-success w-100">Click to make a Donation
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div className="col-lg-8 col-md-7">

            </div>
            <div className="col-lg-4 col-md-5 col-padding-3">

            </div>
        </div>
    );
};

export default SchoolPerformance;