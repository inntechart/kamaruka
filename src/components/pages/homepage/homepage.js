import React from 'react';
import "./homepage.scss";
import arrow from "../../../../public/images/icons/arrow.png";

const Homepage = () => {
    const toTop = () => {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth' });
    };

    return (
        <div className="container hero-items position-relative py-5">
            <div className="row">
                <div className="align-items-center col-md-8 d-flex item text-center text-md-left">
                    <div>
                        <h3>Welcome</h3>
                        <p className="mt-4">
                            The Kamaruka Education Centre is an independent intervention school with a modified curriculum
                            for boys who have difficulty coping with the demands of mainstream school due to learning
                            difficulties, behavioural, social, and or emotional issues.
                            Kamaruka is one of a few places that provides an opportunity to successfully continue at school
                            in a positive and nurturing environment.
                        </p>
                        <a href="" className="text-dark">READ MORE</a>
                    </div>
                </div>
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-blue'>
                            <div>
                                <p>Enrolment</p>
                                <p>Process</p>
                            </div>
                        </div>
                        <div className="image bg-img" style={{backgroundImage: "url('/public/images/cover/enrolment-process.png')"}}/>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-green-dark'>
                            <div>
                                <p>Here to Help</p>
                                <p>Positive choice change</p>
                            </div>
                        </div>
                        <div className="image bg-img" style={{backgroundImage: "url('/public/images/cover/here-to-help.png')"}}/>
                    </div>
                </div>
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-orange'>
                            <div>
                                <p>Intervention</p>
                                <p>Program</p>
                            </div>
                        </div>
                        <div className="image bg-img" style={{backgroundImage: "url('/public/images/cover/Intervention.png')"}}/>
                    </div>
                </div>
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-green'>
                            <div>
                                <p>Donations</p>
                            </div>
                        </div>
                        <div className="image bg-img" style={{backgroundImage: "url('/public/images/cover/donations.png')"}}/>
                    </div>
                </div>
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-blue'>
                            <div>
                                <p>Term</p>
                                <p>Dates</p>
                            </div>
                        </div>
                        <div className="image bg-img" style={{backgroundImage: "url('/public/images/cover/term-dates.png')"}}/>
                    </div>
                </div>
                <div className="col-md-8 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-black'>
                            <div>
                                <p>Respect for self</p>
                                <p>Responsibility for one’s actions</p>
                                <p>Respect for others</p>
                            </div>
                        </div>
                        <div className="image"/>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-8">
                    <div className="row">
                        <div className="col-md-12 item">
                            <div className="w-100">
                                <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-green-dark'>
                                    <div>
                                        <p>Modified Curriculum</p>
                                        <p>Tailored for each student</p>
                                    </div>
                                </div>
                                <div className="image"/>
                            </div>
                        </div>
                        <div className="col-md-6 item">
                            <div className="w-100">
                                <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-orange'>
                                    <div>
                                        <p>Literacy</p>
                                        <p>Intervention</p>
                                    </div>
                                </div>
                                <div className="image"/>
                            </div>
                        </div>
                        <div className="col-md-6 item">
                            <div className="w-100 image ">
                                <div className='align-items-center d-flex img-transparent-background justify-content-center text-center'>

                                </div>
                                <div className="image p-5" style={{backgroundImage: "url('/public/images/cover/20-years.png')"}}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 item">
                    <div className="w-100 h-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-blue h-auto'>
                            <div>
                                <p>Results</p>
                            </div>
                        </div>
                        <div className="image"/>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-4 item">
                    <div className="w-100 h-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-dark-blue h-auto'>
                            <div>
                                <p>Our</p>
                                <p>Strategy</p>
                            </div>
                        </div>
                        <div className="image"/>
                    </div>
                </div>
                <div className="col-md-8">
                    <div className="row">

                        <div className="col-md-6 item">
                            <div className="w-100">
                                <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-transparent-green'>
                                    <div>
                                        <p>Payments</p>
                                    </div>
                                </div>
                                <div className="image p-5 bg-img" style={{backgroundImage: "url('/public/images/cover/payments.png')"}}/>
                            </div>
                        </div>
                        <div className="col-md-6 item">
                            <div className="w-100 image ">
                                <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-orange'>

                                    <div>
                                        <p>Map</p>
                                    </div>
                                </div>
                                <div className="image"/>
                            </div>
                        </div>
                        <div className="col-md-12 item">
                            <div className="w-100">
                                <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-green-dark'>
                                    <div>
                                        <p>ADHD & ASD</p>
                                        <p>Self-management for success</p>
                                    </div>
                                </div>
                                <div className="image"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-blue'>
                            <div>
                                <p>Contact</p>
                            </div>
                        </div>
                        <div className="image"/>
                    </div>
                </div>
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center'>

                        </div>
                        <div className="image" style={{backgroundImage: "url('/public/images/cover/kamaruka-logo.png')"}}/>
                    </div>
                </div>
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-orange'>
                            <div>
                                <p>Enquiry</p>
                                <p>Form</p>
                            </div>
                        </div>
                        <div className="image"/>
                    </div>
                </div>
                <div className="col-md-8 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-black'>
                            <div>
                                <p>Kamaruka derives from an</p>
                                <p>aboriginal term meaning</p>
                                <p>“Camp of the spirit children”</p>
                            </div>
                        </div>
                        <div className="image"/>
                    </div>
                </div>
                <div className="col-md-4 item">
                    <div className="w-100">
                        <div className='align-items-center d-flex img-transparent-background justify-content-center text-center img-opacity-dark-blue'>
                            <div>
                                <p>Careers</p>
                            </div>
                        </div>
                        <div className="image"/>
                    </div>
                </div>
            </div>
            <div className="d-none d-md-block to-top text-center" onClick={()=> toTop()}>
                <img src={arrow} alt="arrow"/>
                <p className="mb-0 text-uppercase">top</p>
            </div>
        </div>
    );
};

export default Homepage;