import React from 'react';
import './App.scss';
import Header from "./components/header/header";
import {Route, Switch, Redirect} from "react-router";
import Homepage from "./components/pages/homepage/homepage";
import AboutIntroduction from "./components/pages/about/introduction/introduction"
import AboutMission from "./components/pages/about/mission/mission"
import SchoolPerformance from "./components/pages/about/school-performance/school-performance"
import Footer from "./components/footer/footer";


const App = () => {
    return (
        <div className="container-fluid">
              <Header/>
              <Switch>
                  <Route exact path={"/home"} component={Homepage}/>
                  <Route exact path="/">
                      <Redirect to="/home"/>
                  </Route>
                  <Route exact path={"/about/introduction"} component={AboutIntroduction}/>
                  <Route exact path={"/about/school-performance"} component={SchoolPerformance}/>
                  <Route exact path={"/about/mission"} component={AboutMission}/>
                  <Route exact path={"/about"}>
                      <Redirect to="/about/introduction"/>
                  </Route>
              </Switch>
            <Footer/>
        </div>

    );
};

export default App;
